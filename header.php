<?php
/**
 * The template for displaying the header.
 *
 * @package PCCF
 */

use function PCCF\Helpers\get_partial;
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="theme-color" content="#d23226" />
		<link rel="stylesheet" href="https://use.typekit.net/lnw8ixe.css">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<?php wp_body_open(); ?>
		<div class="site-wrap">
			<button class="menu-toggle -mobile"><span class="screen-reader-text"><?php esc_html_e( 'Menu Toggle', 'pccf' ); ?></span></button>

			<?php get_partial( 'partials/panel-menu' ); ?>

			<?php
				if ( is_front_page() ) :
					get_partial( 'partials/fixed-hero-home' );
				endif;
			?>

			<div class="main-wrap">

				<div class="wrap">

				<?php get_partial( 'partials/primary-navigation' ); ?>

