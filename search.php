<?php
/**
 * The template for displaying search results pages.
 *
 * @package PCCF
 */

get_header(); ?>

<div class="page-wrapper">
	<div class="inner">

		<section class="search-results" itemscope itemtype="https://schema.org/SearchResultsPage">
			<?php if ( have_posts() ) : ?>
				<h2 class="query">
					<?php
					/* translators: the search query */
					printf( esc_html__( 'Search Results for: %s', 'pccf' ), '<span>' . esc_html( get_search_query() ) . '</span>' );
					?>
				</h2>

				<ul class="grid">
				<?php
				while ( have_posts() ) :
					the_post();
					?>

					<li class="card" itemscope itemtype="https://schema.org/Thing">
						<?php if ( has_post_thumbnail() ) : ?>
							<div class="thumbnail">
								<?php the_post_thumbnail(); ?>
							</div>
						<?php endif; ?>

						<h3 class="title">
							<?php the_title( '<span itemprop="name"><a href="' . esc_url( get_permalink() ) . '" itemprop="url">', '</a></span>' ); ?>
						</h3>

						<div itemprop="description">
							<?php the_excerpt(); ?>
						</div>
					</li>

				<?php endwhile; ?>
				</ul>

				<?php the_posts_navigation(); ?>
			<?php endif; ?>
		</section>

	</div>
</div>
<?php
get_footer();
