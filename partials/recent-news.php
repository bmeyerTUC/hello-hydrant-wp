<?php
/**
 * Recent News
 *
 * @package PCCF
 */

 ?>

<?php
$recent_args = [
	'posts_per_page' => 3,
];

$recent_query = new WP_Query( $recent_args );

if ( $recent_query->have_posts() ) :
?>

	<section class="recent-posts">
		<div class="heading">
			<h3 class="sectiontitle"><?php esc_html_e( 'NEWS', 'pccf' ); ?></h3>
			<a class="viewall" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ) ?>"><?php esc_html_e( 'View All', 'pccf' ); ?></a>
		</div>

		<div class="grid">
		<?php
			while ( $recent_query->have_posts() ) :
				$recent_query->the_post();
		?>

				<article class="post">
					<?php if ( has_post_thumbnail() ) : ?>
						<a href="<?php the_permalink(); ?>">
							<div class="thumbnail">
								<?php echo get_the_post_thumbnail('portrait-large'); ?>
							</div>
						</a>
					<?php endif; ?>

					<h4 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

					<span class="date"><time datetime="<?php echo get_the_date( 'Y-m-d' ); ?>"><?php the_date(); ?></time></span>

					<div class="excerpt">
						<a href="<?php the_permalink(); ?>">
							<?php the_excerpt(); ?>
						</a>
					</div>
				</article>

		<?php endwhile; ?>
		</div>
	</section>

<?php
	endif;
	wp_reset_query();
?>
