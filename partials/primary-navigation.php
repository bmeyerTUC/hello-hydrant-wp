<?php
/**
 * Page Navigation
 *
 * @package PCCF
 */
?>

<div class="primary-navigation">
	<div class="inner">
		<?php if ( is_front_page() ) : ?>
			<div class="logo"><a href="/"><img src="<?php echo PCCF_TEMPLATE_URL;?>/assets/images/PCCF-brandmark-collapse.gif"></a></div>
		<?php else : ?>
			<div class="logo"><a href="/"><img src="<?php echo PCCF_TEMPLATE_URL;?>/assets/images/ppcf-logo@2x.png"></a></div>
		<?php endif; ?>
		<?php
		wp_nav_menu( array(
			'theme_location' => 'primary',
			'depth' => 1,
			'container_class' => 'menucontainer'
		) );
		?>

		<?php  if ( ! is_front_page() ) : ?>
			<button class="menu-toggle"><span class="screen-reader-text"><?php esc_html_e( 'Menu Toggle', 'pccf' ); ?></span></button>
		<?php endif; ?>
	</div>
</div>
