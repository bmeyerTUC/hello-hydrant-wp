<?php
/**
 * Footer CTA
 *
 * @package PCCF
 */
?>

<?php if ( get_field( 'page-footer-cta-visible' ) ) : ?>
	<div class="page-footer">
		<div class="inner">
			<div class="footer-cta">
				<a class="headline" href="<?php echo get_field( 'page-footer-cta-link' ); ?>">
					<span><?php echo get_field( 'page-footer-cta-text' ); ?></span>
					<?php pccf_img_icon( 'right-arrow' ); ?>
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
