<?php
/**
 * Flexible Content - Donate
 *
 * @package PCCF
 */

use function PCCF\Helpers\get_partial;

if( have_rows( 'donate_content_blocks' ) ):

	// Loop through rows.
	while ( have_rows( 'donate_content_blocks' ) ) : the_row();

		if( get_row_layout() == 'two-column-layout' ):

			get_partial( 'partials/two-column-layout' );

        elseif( get_row_layout() == 'faq-section' ):

			get_partial( 'partials/faq-section' );

		endif;

	// End loop.
	endwhile;

endif;

?>
