<?php
/**
 * FAQ Section
 *
 * @package PCCF
 */

?>

<section class="faq-section">

	<h3 class="sectiontitle"><?php the_sub_field( 'section_title' ); ?></h3>

	<?php if( have_rows( 'accordion_row' ) ) : ?>

		<ol class="accordion-block">
		<?php
			$i = 0;
			while ( have_rows( 'accordion_row' ) ) : the_row();
		?>

			<li
				class="section"
				x-data="{ expanded: false }"
				:class="{ '-expanded': expanded }"
				@click.away="expanded = false"
				x-on:keydown.enter.away="expanded = false"
			>
				<h4
					tabindex="0"
					id="accordion-<?php echo $i; ?>"
					class="headline"
					aria-controls="section-<?php echo $i; ?>"
					:aria-expanded="expanded ? 'true' : 'false'"
					@click="expanded = !expanded"
					x-on:keydown.enter="expanded = !expanded"
				>
					<?php the_sub_field('heading'); ?>
				</h4>
				<div class="body" id="section-<?php echo $i; ?>" aria-labelledby="accordion-<?php echo $i; ?>">
					<?php the_sub_field('content'); ?>
				</div>
			</li>

		<?php
			$i++;
			endwhile;
		?>

		</ol>

	<?php endif; ?>

</section>
