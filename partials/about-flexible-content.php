<?php
/**
 * Flexible Content - About
 *
 * @package PCCF
 */

use function PCCF\Helpers\get_partial;

if( have_rows( 'about_content_blocks' ) ):

	// Loop through rows.
	while ( have_rows( 'about_content_blocks' ) ) : the_row();

		if( get_row_layout() == 'image-block' ):

			get_partial( 'partials/image-block' );

        elseif( get_row_layout() == 'highlight_statements_2col' ):

			get_partial( 'partials/highlight_statements_2col' );

		elseif( get_row_layout() == 'highlight_statements_3col' ):

			get_partial( 'partials/highlight_statements_3col' );

		elseif( get_row_layout() == 'personnel_section' ):

			get_partial( 'partials/personnel_section' );

		elseif( get_row_layout() == 'file-downloads' ):

			get_partial( 'partials/file-downloads' );

		endif;

	// End loop.
	endwhile;

endif;

?>
