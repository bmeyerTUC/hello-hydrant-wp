<?php
/**
 * Featured Story
 *
 * @package PCCF
 */

?>

<?php
	$id = get_field( 'featured_story' );
	$author = get_the_author_meta( 'display_name', get_post_field( 'post_author', $id ) );
?>

<a class="featured-story" href="<?php echo get_the_permalink( $id ); ?>">

	<?php if ( has_post_thumbnail( $id ) ) : ?>
		<div class="thumbnail">
			<?php echo get_the_post_thumbnail( $id, 'portrait-large' ); ?>
		</div>
	<?php endif; ?>

	<div class="info">
		<h2 class="title"><?php echo get_the_title( $id ); ?></h2>
		<p class="byline">
			<span class="authoredby"><?php esc_html_e( 'Authored by', 'pccf' ); ?></span>
			<span class="author"><?php echo $author; ?> </span><span class="date"><?php echo get_the_date( 'M d, Y' ); ?></span>
		</p>
	</div>

</a>
