<?php
/**
 * Personnel Section
 *
 * @package PCCF
 */

 ?>

<?php
$section_title = get_sub_field( 'section_label' );
$grid_style = get_sub_field( 'grid_style' );
?>

<?php if ( $section_title && $grid_style == 'extended' ) : ?>

	<div class="personnel-grid -extended">

		<h3 class="sectiontitle"><?php echo $section_title; ?></h3>

		<?php if( have_rows( 'personnel_grid' ) ) : ?>

			<div class="grid">

			<?php while ( have_rows('personnel_grid') ) : the_row(); ?>

				<?php $id = get_sub_field( 'personnel_selection' ); ?>

				<div class="card">
					<?php if ( has_post_thumbnail( $id ) ) : ?>
						<div class="thumbnail">
							<?php echo get_the_post_thumbnail( $id, 'portrait-large' ); ?>
						</div>
					<?php endif; ?>

					<div class="info">
						<h3 class="name"><?php echo get_the_title( $id ); ?></h3>
						<?php
							$title = get_field( 'personnel_professional_title', $id );
							$company = get_field( 'personnel_company', $id );
							$contact = get_field( 'personnel_contact_info', $id );
							$content = get_field( 'personnel_bio', $id );
						?>

						<?php if ( $title ) : ?>
							<h4 class="title"><?php echo $title; ?></h4>
						<?php endif; ?>

						<?php if ( $company ) : ?>
							<h4 class="company"><?php echo $company; ?></h4>
						<?php endif; ?>

						<div class="bio">
							<?php echo $content; ?>
						</div>

					</div>

					<?php if ( $contact ) : ?>
						<div class="contact"><?php echo $contact; ?></div>
					<?php endif; ?>

				</div>

			<?php endwhile; ?>

			</div>

		<?php endif; ?>

	</div>

<?php elseif ( $section_title ) : ?>

	<div class="personnel-grid">

		<h3 class="sectiontitle"><?php echo $section_title; ?></h3>

		<?php if( have_rows( 'personnel_grid' ) ) : ?>

			<div class="grid">

			<?php while ( have_rows('personnel_grid') ) : the_row(); ?>

				<?php $id = get_sub_field( 'personnel_selection' ); ?>

				<div class="card">
					<?php if ( has_post_thumbnail( $id ) ) : ?>
						<div class="thumbnail">
							<?php echo get_the_post_thumbnail( $id, 'portrait-large' ); ?>
						</div>
					<?php endif; ?>

					<div class="info">
						<h3 class="name"><?php echo get_the_title( $id ); ?></h3>
						<?php
							$title = get_field( 'personnel_professional_title', $id );
							$company = get_field( 'personnel_company', $id );
							$contact = get_field( 'personnel_contact_info', $id );
						?>

						<?php if ( $title ) : ?>
							<h4 class="title"><?php echo $title; ?></h4>
						<?php endif; ?>

						<?php if ( $company ) : ?>
							<h4 class="company"><?php echo $company; ?></h4>
						<?php endif; ?>

					</div>

				</div>

			<?php endwhile; ?>

			</div>

		<?php endif; ?>

	</div>

<?php endif; ?>
