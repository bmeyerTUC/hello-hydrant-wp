<?php
/**
 * Stats Highlight
 *
 * @package PCCF
 */

 ?>

<?php if( have_rows( 'stat-cards' ) ): ?>

	<div class="stat-cards">

	<?php while ( have_rows('stat-cards') ) : the_row(); ?>

		<div class="card">
			<p class="figure">
				<?php the_sub_field('figure'); ?>
			</p>
			<p class="description">
				<?php the_sub_field('figure_description'); ?>
			</p>
		</div>

	<?php endwhile; ?>

   </div>

<?php endif; ?>
