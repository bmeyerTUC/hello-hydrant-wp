<?php
/**
 * Hero Lead
 *
 * @package PCCF
 */
?>

<?php
	if ( get_field( 'hero_lead_page_label' ) ) :
		$page_title = get_field( 'hero_lead_page_label' );
	else :
		$page_title = get_the_title();
	endif;
?>

<div class="hero-lead">
	<h2 class="title"><?php echo $page_title; ?></h2>
	<div class="summary">
		<?php echo get_field( 'hero_lead_summary_statement' ); ?>
	</div>
</div>
