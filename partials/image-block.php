<?php
/**
 * Image Block
 *
 * @package PCCF
 */
?>

<?php
	$image_id = get_sub_field( 'image-block__image' );
	$image_position = get_sub_field( 'image-block__position' );

	if ( $image_id ) :
?>
	<div class="image-block">
	<?php
		$img_attr = wp_get_attachment_image_src( $image_id, 'page' );
		$img_srcset = wp_get_attachment_image_srcset( $image_id, 'page' );
		$img_sizes = wp_get_attachment_image_sizes( $image_id, 'page' );
	?>
		<img
			src="<?php echo $img_attr[0]; ?>"
			srcset="<?php echo $img_srcset; ?>"
			sizes="<?php echo $img_sizes; ?>"
			width="<?php echo $img_attr[1]; ?>"
			height="<?php echo $img_attr[2]; ?>"
			style="object-position: <?php echo $image_position >= 0 ? $image_position : '50%'; ?>% 50%;"
		>
	</div>
	<?php endif; ?>
