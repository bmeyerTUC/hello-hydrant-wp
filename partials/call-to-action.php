<?php
/**
 * Call To Action
 *
 * @package PCCF
 */

?>

<section class="cta">

	<a class="button -arrow-secondary" href="<?php the_sub_field( 'button_link' ); ?>">
		<?php the_sub_field( 'button_text' ); ?>
	</a>

</section>
