<?php
/**
 * File Downloads
 *
 * @package PCCF
 */

?>

<section class="file-downloads">

	<?php if ( get_sub_field( 'section_label' ) ) : ?>
	<div class="header">
		<h3 class="title"><?php the_sub_field( 'section_label' ); ?></h3>
		<a class="externallink" href="<?php the_sub_field( 'external_link' ); ?>"><?php the_sub_field( 'external_link_label' ); ?></a>
	</div>
	<?php endif; ?>

	<?php if( have_rows( 'file' ) ): ?>

		<div class="grid">

		<?php while ( have_rows('file') ) : the_row(); ?>

			<a class="card" href="<?php the_sub_field( 'file_link' ); ?>">
				<div class="box">
					<h4 class="label"><?php the_sub_field( 'label' ); ?></h4>
				</div>
				<p class="description"><?php the_sub_field( 'description' ); ?></p>
			</a>

		<?php endwhile; ?>

		</div>

	<?php endif; ?>

</section>
