<?php
/**
 * Highlight Statements - 2 Columns
 *
 * @package PCCF
 */

 ?>

<?php if( have_rows( 'highlight_statement' ) ): ?>

	<div class="highlight-statement-2col">

	<?php while ( have_rows('highlight_statement') ) : the_row(); ?>

		<div class="card">
			<h4 class="headline">
				<?php the_sub_field('statement_headline'); ?>
			</h4>
			<p class="text">
				<?php the_sub_field('statement_text'); ?>
			</p>
		</div>

	<?php endwhile; ?>

   </div>

<?php endif; ?>
