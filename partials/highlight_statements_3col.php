<?php
/**
 * Highlight Statements - 3 Columns
 *
 * @package PCCF
 */

 ?>

<?php
$section_title = get_sub_field( 'highlight_statement_section_title' );
?>

<?php if ( $section_title ) : ?>

	<div class="highlight-statement-3col">

		<h3 class="title"><?php echo $section_title; ?></h3>

		<?php if( have_rows( 'highlight_statement' ) ) : ?>

			<div class="grid">

			<?php while ( have_rows('highlight_statement') ) : the_row(); ?>

				<div class="card">
					<h4 class="headline">
						<?php the_sub_field('statement_headline'); ?>
					</h4>
					<p class="text">
						<?php the_sub_field('statement_text'); ?>
					</p>
				</div>

			<?php endwhile; ?>

			</div>

		<?php endif; ?>

	</div>

<?php endif; ?>
