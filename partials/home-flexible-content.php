<?php
/**
 * Flexible Content - Home
 *
 * @package PCCF
 */

use function PCCF\Helpers\get_partial;

// Check value exists.
if( have_rows( 'home_content_blocks' ) ):

	// Loop through rows.
	while ( have_rows( 'home_content_blocks' ) ) : the_row();

		if( get_row_layout() == 'image-block' ):

			get_partial( 'partials/image-block' );

		elseif( get_row_layout() == 'recent-news' ):

			get_partial( 'partials/recent-news' );

		elseif( get_row_layout() == 'stats-highlight' ):

			get_partial( 'partials/stats-highlight' );

		elseif( get_row_layout() == 'text-hero' ):

			get_partial( 'partials/text-hero' );

		elseif( get_row_layout() == 'upcoming-events' ):

			get_partial( 'partials/upcoming-events' );

		endif;

	// End loop.
	endwhile;

endif;
