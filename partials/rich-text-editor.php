<?php
/**
 * Rich Text Editor
 *
 * @package PCCF
 */

?>

<section class="rich-text">

	<?php the_sub_field( 'rich_text_editor' ); ?>

</section>
