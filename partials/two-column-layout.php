<?php
/**
 * Two Column Layout
 *
 * @package PCCF
 */

use function PCCF\Helpers\get_partial;

$layout_options = get_sub_field( 'layout-options' );
?>

	<div class="two-column <?php echo $layout_options; ?>">
		<div class="image">
			<?php get_partial( 'partials/image-block' ); ?>
		</div>
		<div class="content">
			<?php
			get_partial( 'partials/call-to-action' );
			get_partial( 'partials/rich-text-editor' );
			?>
		</div>
	</div>
