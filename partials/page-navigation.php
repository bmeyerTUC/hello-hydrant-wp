<?php
/**
 * Page Navigation
 *
 * @package PCCF
 */
?>

<?php

global $post;

if ( is_page() && $post->post_parent ) :
	// Page is a child
	$parent_ID = $post->post_parent;
	$nav_title = get_the_title( $parent_ID );
else :
	// Page is a parent
	$parent_ID = get_the_ID();
	$nav_title = get_the_title();
endif;
?>

<div class="page-navigation">
	<h2 class="title"><?php echo $nav_title; ?>:</h2>
	<div class="navigation">
	<?php
		wp_list_pages( array(
			'child_of' => $parent_ID,
			'depth' => 1,
			'title_li' => null
		) );
	?>
	</div>
</div>
