<?php
/**
 * Campaign Grid
 *
 * @package PCCF
 */
?>

<?php if ( get_field( 'show-campaigns' ) ) : ?>

	<?php if( have_rows( 'campaign-grid' ) ) : ?>

		<div class="campaign-grid">

			<?php while ( have_rows('campaign-grid') ) : the_row(); ?>

				<article class="card">
					<?php $id = get_sub_field( 'campaigns' ); ?>
					<h3 class="title"><?php echo get_the_title( $id ); ?></h3>
					<?php if ( has_post_thumbnail( $id ) ) : ?>
						<a href="<?php the_permalink( $id ); ?>">
							<div class="thumbnail">
								<?php echo get_the_post_thumbnail( $id, 'landscape-small' ); ?>
							</div>
						</a>
					<?php endif; ?>
					<p class="excerpt">
						<?php echo get_the_excerpt( $id ); ?>
					</p>
					<a class="readmore" href="<?php the_permalink( $id ); ?>"><?php esc_html_e( 'support now' ); ?></a>
				</article>

			<?php endwhile; ?>

		</div>

	<?php endif; ?>

<?php endif; ?>
