<?php
/**
 * Fixed Hero
 *
 * @package PCCF
 */
?>

<div class="fixed-hero">
	<div class="background">
		<?php the_post_thumbnail( get_the_id(), 'hero-large' ); ?>
	</div>
	<div class="inner">
		<button class="menu-toggle"><span class="screen-reader-text">Menu Toggle</span></button>
		<div class="logo"><img src="<?php echo PCCF_TEMPLATE_URL;?>/assets/images/pccf-logo-white@2x.png" ></div>
		<div class="tagline"><img src="<?php echo PCCF_TEMPLATE_URL;?>/assets/images/thrive@2x.png" ></div>
	</div>
</div>
