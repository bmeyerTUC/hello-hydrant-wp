<?php
/**
 * Email signup
 *
 * @package PCCF
 */
?>

<div id="mc_embed_signup">
	<form action="https://pimafoundation.us19.list-manage.com/subscribe/post?u=85b5b568ab1047e2cbc3eeeff&amp;id=1182413165" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
		<div id="mc_embed_signup_scroll">

		<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required="">
		<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
		<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_85b5b568ab1047e2cbc3eeeff_1182413165" tabindex="-1" value=""></div>
		<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
		</div>
	</form>
</div>
