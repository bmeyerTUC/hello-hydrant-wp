<div class="panel-menu">
	<div class="header">
		<div class="logo">
			<a href="/"><img src="<?php echo PCCF_TEMPLATE_URL;?>/assets/images/pccf-logo-white@2x.png"></a></div>
		<a href="https://www.pima.edu/" target="_blank" class="button -outline -external"><?php esc_html_e( 'Pima.edu', 'pccf' ); ?></a>
		<button class="close-menu"><span class="screen-reader-text"><?php esc_html_e( 'Close Menu', 'pccf' ); ?></span></button>
	</div>

	<div class="main">
		<?php get_search_form(); ?>
		<?php
			wp_nav_menu( array(
				'theme_location' => 'panel',
				'depth' => 2,
				'container_class' => 'menucontainer'
			) );
		?>
	</div>

	<div class="footer">
		<a href="https://www.pima.edu/" target="_blank" class="button -outline -external"><?php esc_html_e( 'Pima.edu', 'pccf' ); ?></a>
		<?php
		// TODO: Move into theme options
		$social = array(
			'facebook' => 'https://www.facebook.com/pimafoundation/',
			'instagram' => 'https://www.instagram.com/pimafoundation/',
			'linkedin' => 'https://www.linkedin.com/company/pimafoundation',
			'twitter' => 'https://twitter.com/PimaFoundation'
		)
		?>
		<ul class="social-links">
			<?php foreach ($social as $key => $value) : ?>
				<li><a href="<?php esc_html_e( $value, 'pccf' ); ?>" target="_blank"><?php pccf_img_icon( $key . '-cyan-icon' ); ?></a></li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>
