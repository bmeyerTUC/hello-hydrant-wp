<?php
/**
 * Upcoming Events
 *
 * @package PCCF
 */

use function PCCF\Helpers\get_partial_options;

// Partial options passed over.
$opts = get_partial_options();

$events_to_show = is_array( $opts ) && isset( $opts['events_to_show'] )
	? $opts['events_to_show']
	: 4;
?>

<?php
$event_date = current_time( 'Ymd' );

$events_args = array(
	'posts_per_page' 	=> $events_to_show,
	'post_type' 		=> 'event',
	'meta_key' 			=> 'event_date',
	'meta_query' 	=> array(
		'key' 		=> 'event_date',
		'value'		=> $event_date,
		'compare'	=> '>=',
		'type'		=> 'DATE'
	),
	'orderby' => 'meta_value',
	'order' => 'ASC'
);

$events_query = new WP_Query( $events_args );

if ( $events_query->have_posts() ) :
	$post_quantity = $events_query->found_posts;

	$decorator = $post_quantity > 4 && $events_to_show > 4 ? '-decorator' : '';
?>

	<section class="upcoming-events">
		<div class="heading">
			<h3 class="sectiontitle"><?php esc_html_e( 'EVENTS', 'pccf' ); ?></h3>
			<a class="viewall" href="<?php echo esc_url('/event'); ?>"><?php esc_html_e( 'View All', 'pccf' ); ?></a>
		</div>

		<div class="grid <?php echo $decorator; ?>">
		<?php
			while ( $events_query->have_posts() ) :
				$events_query->the_post();
		?>

				<article class="event">
					<?php
						// Convert the date from Ymd to d M Y
						// e.g. 20200530 to 30 May 2020
						$event_date = get_field( 'event_date' );

						$event_year = substr($event_date, 0, 4);
						$event_mon = substr($event_date, 4, 2);
						$event_mon = date( "M", mktime(0, 0, 0, $event_mon, 10) );
						$event_day = substr($event_date, 6, 2);

						$date_formatted = [ $event_day, $event_mon, $event_year ];
						$date_formatted = implode( ' ', $date_formatted );
					?>

					<span class="date"><time datetime="<?php echo $event_date; ?>"><?php echo $date_formatted; ?></time></span>
					<h4 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

					<?php $location = get_field( 'event_location' ); ?>

					<?php if ( $location ) : ?>
						<div class="location"><?php echo $location; ?></div>
					<?php endif; ?>
				</article>
		<?php endwhile; ?>
		</div>
	</section>

<?php
	endif;
	wp_reset_query();
?>
