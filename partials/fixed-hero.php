<?php
/**
 * Fixed Hero
 *
 * @package PCCF
 */
?>


<?php if ( has_post_thumbnail() ) : ?>
<div class="fixed-hero -short">
	<div class="background">
		<?php the_post_thumbnail( get_the_id(), 'hero-large' ); ?>
	</div>
</div>
<?php endif; ?>
