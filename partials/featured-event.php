<?php
/**
 * Featured Event
 *
 * @package PCCF
 */

?>

<?php
	$id = get_field( 'featured_event' );
?>

<article class="featured-event">


	<?php if ( has_post_thumbnail( $id ) ) : ?>
		<a class="thumbnail" href="<?php echo get_the_permalink( $id ); ?>">
			<?php echo get_the_post_thumbnail( $id, 'portrait-large' ); ?>
		</a>
	<?php endif; ?>

	<?php
		// Convert the date from Ymd to d M Y
		// e.g. 20200530 to 30 May 2020
		$event_date = get_field( 'event_date', $id );

		$event_year = substr($event_date, 0, 4);
		$event_mon = substr($event_date, 4, 2);
		$event_mon = date( "M", mktime(0, 0, 0, $event_mon, 10) );
		$event_day = substr($event_date, 6, 2);

		$date_formatted = [ $event_day, $event_mon, $event_year ];
		$date_formatted = implode( ' ', $date_formatted );
	?>

	<div class="info">
		<span class="date"><time datetime="<?php echo $event_date; ?>"><?php echo $date_formatted; ?></time></span>
		<h4 class="title"><a href="<?php get_the_permalink( $id ); ?>"><?php echo get_the_title( $id ); ?></a></h4>

		<?php $location = get_field( 'event_location', $id ); ?>

		<?php if ( $location ) : ?>
			<div class="location"><?php echo $location; ?></div>
		<?php endif; ?>
	</div>

</article>
