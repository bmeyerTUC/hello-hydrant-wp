<?php
/**
 * Text Hero
 *
 * @package PCCF
 */

$text_hero = get_sub_field( 'text-hero__text' );

if ( $text_hero ) :
?>

	<div class="text-hero"><?php echo $text_hero; ?></div>

<?php endif; ?>
