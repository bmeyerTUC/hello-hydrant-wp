<?php
/**
 * Core setup, site hooks and filters.
 *
 * @package PCCF\Core
 */

namespace PCCF\Core;

/**
 * Set up theme defaults and register supported WordPress features.
 *
 * @return void
 */
function setup() {
	$n = function( $function ) {
		return __NAMESPACE__ . "\\$function";
	};

	add_action( 'after_setup_theme', $n( 'i18n' ) );
	add_action( 'after_setup_theme', $n( 'theme_setup' ) );
	add_action( 'wp_enqueue_scripts', $n( 'scripts' ) );
	add_action( 'wp_enqueue_scripts', $n( 'styles' ) );
	add_action( 'wp_head', $n( 'js_detection' ), 0 );
	add_action( 'wp_head', $n( 'add_manifest' ), 10 );

	add_filter( 'script_loader_tag', $n( 'script_loader_tag' ), 10, 2 );
}

/**
 * Makes Theme available for translation.
 *
 * Translations can be added to the /languages directory.
 * If you're building a theme based on "PCCF", change the
 * filename of '/languages/PCCF.pot' to the name of your project.
 *
 * @return void
 */
function i18n() {
	load_theme_textdomain( 'PCCF', PCCF_PATH . '/languages' );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
function theme_setup() {
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'hero-large', 1800, 973 );
	add_image_size( 'hero-medium', 1396, 785, true );
	add_image_size( 'landscape', 904, 640, true );
	add_image_size( 'landscape-small', 904, 374, true );
	add_image_size( 'page', 1396, 1100 );
	add_image_size( 'portrait-small', 375, 475, true );
	add_image_size( 'portrait-large', 900, 900, true );

	add_theme_support(
		'html5',
		array(
			'search-form',
			'gallery',
		)
	);

	add_post_type_support( 'page', 'excerpt' );

	// This theme uses wp_nav_menu() in three locations.
	register_nav_menus(
		array(
			'panel' => esc_html__( 'Panel Menu', 'pccf' ),
			'primary' => esc_html__( 'Primary Menu', 'pccf' ),
			'footer' => esc_html__( 'Footer Menu', 'pccf' ),
			'disclosures' => esc_html__( 'Footer Disclosure Menu', 'pccf' ),
		)
	);
}

/**
 * Enqueue scripts for front-end.
 *
 * @return void
 */
function scripts() {

	wp_enqueue_script(
		'frontend',
		PCCF_TEMPLATE_URL . '/dist/js/frontend.js',
		[],
		PCCF_VERSION,
		true
	);

	if ( is_page_template( 'templates/page-styleguide.php' ) ) {
		wp_enqueue_script(
			'styleguide',
			PCCF_TEMPLATE_URL . '/dist/js/styleguide.js',
			[],
			PCCF_VERSION,
			true
		);
	}

}

/**
 * Enqueue styles for front-end.
 *
 * @return void
 */
function styles() {

	wp_enqueue_style(
		'styles',
		PCCF_TEMPLATE_URL . '/dist/css/style.css',
		[],
		PCCF_VERSION
	);

	if ( is_page_template( 'templates/page-styleguide.php' ) ) {
		wp_enqueue_style(
			'styleguide',
			PCCF_TEMPLATE_URL . '/dist/css/styleguide-style.css',
			[],
			PCCF_VERSION
		);
	}
}

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @return void
 */
function js_detection() {

	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}

/**
 * Add async/defer attributes to enqueued scripts that have the specified script_execution flag.
 *
 * @link https://core.trac.wordpress.org/ticket/12009
 * @param string $tag    The script tag.
 * @param string $handle The script handle.
 * @return string
 */
function script_loader_tag( $tag, $handle ) {
	$script_execution = wp_scripts()->get_data( $handle, 'script_execution' );

	if ( ! $script_execution ) {
		return $tag;
	}

	if ( 'async' !== $script_execution && 'defer' !== $script_execution ) {
		return $tag;
	}

	// Abort adding async/defer for scripts that have this script as a dependency. _doing_it_wrong()?
	foreach ( wp_scripts()->registered as $script ) {
		if ( in_array( $handle, $script->deps, true ) ) {
			return $tag;
		}
	}

	// Add the attribute if it hasn't already been added.
	if ( ! preg_match( ":\s$script_execution(=|>|\s):", $tag ) ) {
		$tag = preg_replace( ':(?=></script>):', " $script_execution", $tag, 1 );
	}

	return $tag;
}

/**
 * Appends a link tag used to add a manifest.json to the head
 *
 * @return void
 */
function add_manifest() {
	echo "<link rel='manifest' href='" . esc_url( PCCF_TEMPLATE_URL . '/manifest.json' ) . "' />";
}
