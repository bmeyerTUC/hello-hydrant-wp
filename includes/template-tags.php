<?php
/**
 * Custom template tags for this theme.
 *
 * This file is for custom template tags only and it should not contain
 * functions that will be used for filtering or adding an action.
 *
 * All functions should be prefixed with pccf in order to prevent
 * pollution of the global namespace and potential conflicts with functions
 * from plugins.
 * Example: `pccf_function()`
 *
 * @package PCCF\Template_Tags
 *
 */

// phpcs:ignoreFile

/**
 * Output an SVG
 *
 * @param string  $name The name of the icon to get. Same as the ID.
 * @param array   $options Options for the icon.
 * @param boolean $return Whether to return rather than echo.
 *
 * @return string|void String containing SVG w/ external reference, or void
 */

function pccf_img_icon( $name, $options = array(), $return = false ) {
	$href = PCCF_TEMPLATE_URL . '/assets/svg/' . $name . '.svg';

	$class  = empty( $options['class'] ) ? 'icon icon--' . $name : $options['class'];
	$label = empty( $options['label'] ) ? '' : $options['label'];

	$str    = sprintf( '<img class="%s" alt="%s" src="%s">', esc_attr( $class ), esc_attr( $label ), esc_url( $href ) );
	if ( $return ) {
		return $str;
	}

	echo $str;
}
