<?php
/**
 * Helper functions
 *
 * @package PCCF
 */

namespace PCCF\Helpers;

/**
 * Get partial
 *
 * @param string $path Partial path.
 * @param array  $opts Options to pass to the partial.
 */
function get_partial( $path, $opts = [] ) {
	$GLOBALS['get_partial_opts'] = $opts;
	get_template_part( $path, '' );
	unset( $GLOBALS['get_partial_opts'] );
}

/**
 * Get partial options
 *
 * @return array|mixed
 */
function get_partial_options() {
	if ( ! empty( $GLOBALS['get_partial_opts'] ) ) {
		return $GLOBALS['get_partial_opts'];
	} else {
		return [];
	}
}
