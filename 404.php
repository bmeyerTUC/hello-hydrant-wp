<?php
/**
 * The template for displaying 404 pages (Not Found)
 */

get_header(); ?>

	<div class="page-wrapper">
		<div class="inner">

		<div class="the-404">
			<h2><?php _e( '404', 'pccf' ); ?></h2>
			<p><?php _e( 'Looks like you reached this page in error. Maybe try a search?', 'pccf' ); ?></p>

			<?php get_search_form(); ?>
		</div>

		</div>
	</div>

<?php get_footer(); ?>
