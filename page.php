<?php
/**
 * The default page template
 *
 * @package PCCF
 */

use function PCCF\Helpers\get_partial;

get_header(); ?>

	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>

				<div class="page-single">
					<h2 class="page-title"><?php the_title(); ?></h2>
					<?php if ( has_post_thumbnail() ) : ?>
						<div class="thumbnail">
							<?php the_post_thumbnail( 'page' ); ?>
						</div>
					<?php endif; ?>
					<?php the_content(); ?>
					</div>
			</div>

		<?php endwhile; ?>
	<?php endif; ?>

<?php
get_footer();
