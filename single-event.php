<?php
/**
 * Single Event
 *
 * @package PCCF
 */

use function PCCF\Helpers\get_partial;

get_header(); ?>

	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<div class="post-layout">
				<div class="header">
					<div class="inner">
						<p class="date">
						<?php
						// Convert the date from Ymd to d M Y
						// e.g. 20200530 to 30 May 2020
						$event_date = get_field( 'event_date' );

						$event_year = substr($event_date, 0, 4);
						$event_mon = substr($event_date, 4, 2);
						$event_mon = date( "M", mktime(0, 0, 0, $event_mon, 10) );
						$event_day = substr($event_date, 6, 2);

						$date_formatted = [ $event_day, $event_mon, $event_year ];
						$date_formatted = implode( ' ', $date_formatted );
					?>

					<span class="date"><time datetime="<?php echo $event_date; ?>"><?php echo $date_formatted; ?></time></span>

					</p>
						<h2 class="title"><?php the_title(); ?></h2>
					</div>
				</div>
				<div class="main">
					<div class="inner">
						<div class="byline">
							<?php if ( get_field( 'event_location' ) ) : ?>
							<div class="row">
								<span><?php esc_html_e( 'Venue', 'pccf' );?></span>
								<?php the_field('event_location'); ?>
							</div>
							<?php endif; ?>
							<?php
								$contact = get_field( 'event_contact' );
								if ( $contact ) :
							?>
								<div class="row">
									<span><?php esc_html_e( 'Contact', 'pccf' );?></span>
									<a href="<?php esc_attr( 'mailto:' . $contact ); ?>"><?php echo $contact; ?></a>
								</div>
							<?php endif; ?>
							<?php
								$event_cta = get_field( 'event_cta' );
								if ( $event_cta ) :
							?>
								<div class="row">
									<?php foreach( $event_cta as $event ) : ?>
										<?php if ( $event['button_type'] === 'calendar' ): ?>
											<a href="#add-to-calendar" class="button -plus"><?php esc_html_e( 'Add to Cal', 'pccf' ); ?></a>
										<?php endif; ?>

										<?php if ( $event['button_type'] === 'link' ): ?>
											<a href="<?php echo esc_url( $event['cta_link'] );?>" target="_blank" class="button -arrow"><?php esc_html_e( $event['cta_label'], 'pccf' ); ?></a>
										<?php endif; ?>

										<?php if ( $event['button_type'] === 'registration' ): ?>
											<a href="<?php echo esc_url( $event['cta_link'] ); ?>" data-blackbaud="<?php echo $event['blackbaud_id']; ?>" class="button -arrow-secondary blackbaud-trigger"><?php esc_html_e( 'Register Now', 'pccf' ); ?></a>
										<?php endif; ?>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
						</div>
						<?php if ( has_post_thumbnail() ) : ?>
							<div class="thumbnail">
								<?php the_post_thumbnail( 'page' ); ?>
							</div>
						<?php endif; ?>
						<div class="content">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>

			<?php get_partial( 'partials/footer-cta' ); ?>

		<?php endwhile; ?>
	<?php endif; ?>

<?php
get_footer();
