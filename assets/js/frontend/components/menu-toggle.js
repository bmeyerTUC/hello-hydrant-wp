/**
 * If elements found, attach event listeners, else exit
 */
const init = () => {

	const menuToggle = document.querySelectorAll( '.menu-toggle' );
	const closeMenu = document.querySelectorAll( '.close-menu' );

	if ( !menuToggle ) {
		return;
	}

	menuToggle.forEach( ( el ) => {
		el.addEventListener( 'click', ( e ) => {
			togglePanelMenu( e.target );
		} );
	} );

	closeMenu.forEach( ( el ) => {
		el.addEventListener( 'click', ( e ) => {
			closePanelMenu( e.target );
		} );
	} );

};

/**
 * Toggle menu ui
 * @param  {} toggle
 * @param  {} menu
 */
const togglePanelMenu = ( toggle ) => {

	const menu = document.querySelector( '.panel-menu' );

	if ( menu.classList.contains( '-open' ) ) {

		closePanelMenu( toggle );

	} else {

		document.body.classList.add( '-menu-open' );

		menu.classList.add( '-open' );

		toggle.setAttribute( 'menu-hidden', false );

	}

};

/**
 * Close Panel Menu
 */
const closePanelMenu = ( toggle ) => {

	const menu = document.querySelector( '.panel-menu' );

	document.body.classList.remove( '-menu-open' );
	menu.classList.remove( '-open' );
	toggle.setAttribute( 'menu-hidden', true );

};

export { init };
