// Imports
import 'alpinejs';
import { init as menuToggle } from './components/menu-toggle';

// Instantiation
menuToggle();
