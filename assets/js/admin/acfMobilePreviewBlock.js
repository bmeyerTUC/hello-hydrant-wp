var init = function() {

	var mobilePreviewEl = document.querySelectorAll( '.acf-field.mobile-preview' );

	if ( !mobilePreviewEl ) {
		return;
	}

    if ( mobilePreviewEl ) {
        for ( var i = 0; i < mobilePreviewEl.length; i++ ) {

			mobilePreviewEl[i].querySelector( '.acf-actions' ).style.display = "none";
			mobilePreviewEl[i].querySelector( '.acf-th' ).style.display = "none";

            var adjustmentEl = mobilePreviewEl[i].nextElementSibling;

            if ( adjustmentEl.classList.contains( '.acf-field-range.mobile-alignment' ) ) {
                managePreview( mobilePreviewEl[i], adjustmentEl );
            }
        }
    }

};

var managePreview = function( previewEl, adjustmentEl ) {

    if ( previewEl && adjustmentEl ) {
        adjustmentEl.addEventListener( 'change', function() {
            previewEl.style.objectPosition = this.value + '% 50%';
        });
    }

}

window.addEventListener('load', function(event) {
	init();
} );
