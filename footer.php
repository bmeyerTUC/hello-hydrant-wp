<?php
/**
 * The template for displaying the footer.
 *
 * @package PCCF
 */

use function PCCF\Helpers\get_partial;
?>
				</div> <!-- end .main-wrap .wrap -->
			</div> <!-- end .main-wrap -->

			<div class="site-footer">

				<div class="wrap">

					<div class="upper">
						<div class="col">
							<div class="logo"><a href="/"><img src="<?php echo PCCF_TEMPLATE_URL;?>/assets/images/ppcf-logo@2x.png"></a></div>
						</div>
						<div class="col">
							<div class="footer-nav">
								<?php
								wp_nav_menu( array(
									'theme_location' => 'footer',
									'depth' => 1,
									'container_class' => 'menucontainer'
								) );
								?>
							</div>
						</div>
						<div class="col">
							<div class="signup">

								<p><?php esc_html_e( 'Get updates on the latest news from Pima Community College Foundation.', 'pccf' ); ?></p>

								<?php get_partial( 'partials/email-signup' ); ?>

							</div>
							<p class="address">
								<!-- TODO: move into theme options -->
								Pima Community College Foundation<br/>
								4905C E. Broadway Blvd, Ste 252<br/>
								Tucson, Arizona 85709<br/>
							</p>
							<p class="address">
								<!-- TODO: move into theme options -->
								<a href="mailto:hello@pimafoundation.org">hello@pimafoundation.org</a><br/>
								Office: 520 206 4646<br/>
								Fax: 520 206 4648<br/>
							</p>
						</div>
					</div>

					<div class="lower">
						<div class="left">
							<p>&copy; Pima Community College Foundation. All rights reserved.</p>
							<?php
								wp_nav_menu( array(
									'theme_location' => 'disclosures',
									'depth' => 1,
									'container_class' => 'menucontainer'
								) );
							?>
						</div>
						<div class="right">

						<?php
						// TODO: Move into theme options
						$social = array(
							'facebook' => 'https://www.facebook.com/pimafoundation/',
							'instagram' => 'https://www.instagram.com/pimafoundation/',
							'linkedin' => 'https://www.linkedin.com/company/pimafoundation',
							'twitter' => 'https://twitter.com/PimaFoundation',
						)
						?>
							<ul class="social-links">
								<?php foreach ($social as $key => $value) : ?>
									<li><a href="<?php esc_html_e( $value, 'pccf' ); ?>" target="_blank"><?php pccf_img_icon( $key . '-icon' ); ?></a></li>
								<?php endforeach; ?>
							</ul>
						</div>
					</div>

				</div>
			</div>
			<?php wp_footer(); ?>
		</div> <!-- end .site-wrap -->
	</body>
</html>
