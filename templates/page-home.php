<?php
/**
 * Template Name: Home Page
 *
 * @package PCCF
 */

get_header();

use function PCCF\Helpers\get_partial;
?>

	<div class="page-wrapper">
		<div class="inner">
			<?php get_partial( 'partials/home-flexible-content' ); ?>
		</div>
	</div>

	<?php get_partial( 'partials/footer-cta' ); ?>

<?php get_footer(); ?>
