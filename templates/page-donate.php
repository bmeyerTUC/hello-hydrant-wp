<?php
/**
 * Template Name: Donate Pages
 *
 * @package PCCF
 */

get_header();

use function PCCF\Helpers\get_partial;
?>

	<div class="page-wrapper">
		<div class="inner">
			<?php get_partial( 'partials/page-navigation' ); ?>
			<?php get_partial( 'partials/hero-lead' ); ?>
			<?php get_partial( 'partials/donate-flexible-content' ); ?>
			<?php get_partial( 'partials/campaign-grid' ); ?>
		</div>
	</div>

	<?php get_partial( 'partials/footer-cta' ); ?>

<?php get_footer(); ?>
