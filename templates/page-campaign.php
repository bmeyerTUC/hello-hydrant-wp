<?php
/**
 * Template Name: Donate Campaigns
 *
 * @package PCCF
 */

get_header();

use function PCCF\Helpers\get_partial;
?>

	<?php get_partial( 'partials/fixed-hero' ); ?>

	<div class="page-wrapper">
		<div class="inner -narrow">
			<h2 class="page-title"><?php the_title(); ?></h2>
			<?php get_partial( 'partials/donate-flexible-content' ); ?>
		</div>

		<?php get_partial( 'partials/footer-cta' ); ?>
	</div>

<?php get_footer(); ?>
