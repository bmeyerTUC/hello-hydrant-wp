<?php
/**
 * Template Name: Events Landing
 *
 * @package PCCF
 */

get_header();

use function PCCF\Helpers\get_partial;
?>

	<div class="page-wrapper">
		<div class="inner">
		<?php
			get_partial( 'partials/hero-lead' );
			get_partial( 'partials/featured-story' );
			get_partial( 'partials/featured-event' );
			get_partial(
				'partials/upcoming-events',
				[
					'events_to_show' => 8
				]
			);
		?>
		</div>
	</div>

	<?php get_partial( 'partials/footer-cta' ); ?>

<?php get_footer(); ?>
