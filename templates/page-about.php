<?php
/**
 * Template Name: About Pages
 *
 * @package PCCF
 */

get_header();

use function PCCF\Helpers\get_partial;
?>

	<?php get_partial( 'partials/fixed-hero' ); ?>

	<div class="page-wrapper">
		<div class="inner">
			<?php get_partial( 'partials/page-navigation' ); ?>
			<?php get_partial( 'partials/hero-lead' ); ?>
			<?php get_partial( 'partials/about-flexible-content' ); ?>
		</div>
	</div>

	<?php get_partial( 'partials/footer-cta' ); ?>

<?php get_footer(); ?>
