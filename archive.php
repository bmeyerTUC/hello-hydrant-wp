<?php
/**
 * The template for displaying search results pages.
 *
 * @package PCCF
 */

get_header(); ?>

<div class="page-wrapper">
	<div class="inner">

		<?php the_archive_title( '<h1 class="page-title">', '</h1>' ); ?>

		<section class="search-results">
			<?php if ( have_posts() ) : ?>

				<ul class="grid">
				<?php
				while ( have_posts() ) :
					the_post();
					?>

					<li class="card" itemscope itemtype="https://schema.org/Thing">
						<?php if ( has_post_thumbnail() ) : ?>
							<div class="thumbnail">
								<?php the_post_thumbnail(); ?>
							</div>
						<?php endif; ?>

						<h3 class="title">
							<?php the_title( '<span itemprop="name"><a href="' . esc_url( get_permalink() ) . '" itemprop="url">', '</a></span>' ); ?>
						</h3>

						<div itemprop="description">
							<?php the_excerpt(); ?>
						</div>
					</li>

				<?php endwhile; ?>
				</ul>

				<?php the_posts_navigation(); ?>
			<?php endif; ?>
		</section>

	</div>
</div>
<?php
get_footer();
