<?php
/**
 * The main template file
 *
 * @package PCCF
 */

use function PCCF\Helpers\get_partial;

get_header(); ?>

	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<div class="post-layout">
				<div class="header">
					<div class="inner">
						<p class="date"><time datetime="<?php echo get_the_date( 'Y-m-d' ); ?>"><?php the_date( 'd M Y' ); ?></time></p>
						<h2 class="title"><?php the_title(); ?></h2>
					</div>
				</div>
				<div class="main">
					<div class="inner">
						<div class="byline">
							<p class="row">
								<span><?php esc_html_e( 'Authored By', 'pccf' );?></span>
								<?php the_author(); ?>
							</p>
							<?php if ( $meta = get_the_author_meta( 'user_email' ) ) :?>
								<p class="row">
									<span><?php esc_html_e( 'Contact', 'pccf' );?></span>
									<a href="<?php esc_attr( 'mailto:' . $meta ); ?>"><?php echo $meta; ?></a>
								</p>
							<?php endif; ?>
						</div>
						<?php if ( has_post_thumbnail() ) : ?>
							<div class="thumbnail">
								<?php the_post_thumbnail( 'page' ); ?>
							</div>
						<?php endif; ?>
						<div class="content">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>

			<?php get_partial( 'partials/footer-cta' ); ?>

		<?php endwhile; ?>
	<?php endif; ?>

<?php
get_footer();
