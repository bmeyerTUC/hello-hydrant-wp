<?php
/**
 * WP Theme constants and setup functions
 *
 * @package PCCF
 */

// Useful global constants.
define( 'PCCF_VERSION', '0.1.0' );
define( 'PCCF_TEMPLATE_URL', get_template_directory_uri() );
define( 'PCCF_PATH', get_template_directory() . '/' );
define( 'PCCF_INC', PCCF_PATH . 'includes/' );

// for use with wp_kses, applied to svg's
define( 'SVG_TAGS_ALLOW', 'svg,use,symbol,g,path' );
define( 'SVG_ATTR_ALLOW', 'id,class,role,aria-label,aria-hidden,version,xmlns,xmlns:xlink,xlink:href,width,height,viewBox,d,fill,fill-rule,transform' );

require_once PCCF_INC . 'core.php';
require_once PCCF_INC . 'overrides.php';
require_once PCCF_INC . 'template-tags.php';
require_once PCCF_INC . 'helpers.php';
require_once PCCF_INC . 'utility.php';
require_once PCCF_INC . 'blocks.php';

// Enable JSON syncing for Advanced Custom Fields
require_once PCCF_INC . 'ACF.php';

// Run the setup functions.
PCCF\Core\setup();
PCCF\Blocks\setup();

require_once PCCF_INC . 'CPT.php';

// Require Composer autoloader if it exists.
if ( file_exists( __DIR__ . '/vendor/autoload.php' ) ) {
	require_once 'vendor/autoload.php';
}




if ( ! function_exists( 'wp_body_open' ) ) {

	/**
	 * Shim for the the new wp_body_open() function that was added in 5.2
	 */
	function wp_body_open() {
		do_action( 'wp_body_open' );
	}
}
